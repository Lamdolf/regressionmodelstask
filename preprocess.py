from pandas import DataFrame
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import normalize
from sklearn.model_selection import train_test_split

def normalizeData(rawData, COLS):

    normalizedData = normalize(rawData)
    normalizedDataFrame = DataFrame(normalizedData)
    normalizedDataFrame.columns = COLS

    print("Detalles de datos normalizados")
    for col in normalizedDataFrame.columns:
        print("+----------------------------------+")
        print(normalizedDataFrame[[col]].describe())
        print("+----------------------------------+")

    return normalizedDataFrame


def dataset_cleaner(rawData, COLS):
    
    print(f"Forma antes de procesado: {rawData.shape}") 

    print("Separación de sets de datos y target sin procesar:")
    rawDataArray = rawData.values
    rawTrain, rawTarget = rawDataArray[:, :-1], rawDataArray[:, -1]

    print(f"Generados set de datos\n"
          f"Entrada: {rawTrain.shape}\n"
          f"Objetivo: {rawTarget.shape}")

    print("Evaluación y filtrado de datos anómalos utilizando método de "
          "Isolation Forest")

    iso = IsolationForest(contamination=0.1)
    anom = iso.fit_predict(rawTrain)
    mask = anom != -1

    cleanTrain, cleanTarget = rawTrain[mask, :], rawTarget[mask]

    print("Forma de datos filtrados:")
    print(f"Set de entrada: {cleanTrain.shape}")
    print(f"Set objetivo: {cleanTarget.shape}")

    print("Detalles de nuevos datos")
    print("Set de entrada:")
    describeTrain = DataFrame(cleanTrain)
    describeTrain.columns = COLS[:-1]
    for col in describeTrain.columns:
        print("+----------------------------------+")
        print(describeTrain[[col]].describe())
        print("+----------------------------------+")

    print("+----------------------------------+")
    print("Set objetivo: ")
    print("Columna: tumor_size")
    print(DataFrame(cleanTarget).describe())
    print("+----------------------------------+")

    
    return cleanTrain, cleanTarget


def generateSamples(cleanTrain, cleanTarget):

    print("Generando datos de entrenamiento y de pruebas")
    trainInput, testInput, trainTarget, testTarget = train_test_split(cleanTrain,
                                                                      cleanTarget,
                                                                      test_size=0.2)

    print("Sets generados para entrenamiento:")
    print(f"Input: {trainInput.shape}")
    print(f"Target: {trainTarget.shape}\n")
    print("Sets generados para pruebas:")
    print(f"Input: {testInput.shape}")
    print(f"Target: {testTarget.shape}")

    return trainInput, trainTarget, testInput, testTarget
