from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_validate
from sklearn.metrics import mean_squared_error
from statistics import mean, pstdev
from pandas import DataFrame


def negate(a):
    return a*-1


def crossValidation(model, modelname, x, y):
    
    print(f"Realizando validación cruzada con k=5 en modelo: {modelname}")
    cv_results = cross_validate(model, x, y, cv=5,
                                scoring="neg_mean_squared_error",
                                return_train_score=True,
                                n_jobs=5)
    
    for key in cv_results:
        print(key)
        print(cv_results[key])

    testScores = list(map(negate, cv_results["test_score"]))
    scoresMean = mean(testScores)
    scoresStd = pstdev(testScores)
    print(f"MSE promedio: {scoresMean}")
    print(f"Desviación estándar de MSE: {scoresStd}")

    validationSummary = [modelname]
    for val in testScores:
        validationSummary.append(val)

    validationSummary.append(scoresMean)
    validationSummary.append(scoresStd)

    return validationSummary


def linealModel(trainInput, trainTarget, testInput, testTarget):

    print("Generando modelo de regresión lineal")
    model = LinearRegression()
    model.fit(trainInput, trainTarget)
    modelSummary = crossValidation(model, "Lineal Regression", trainInput, trainTarget)
    targetPrediction = model.predict(testInput)
    finalTestError = mean_squared_error(testTarget, targetPrediction)
    modelSummary.append(finalTestError)
    print(f"MSE en validación 80/20: {finalTestError}")

    return modelSummary


def polynomialModel(trainInput, trainTarget, testInput, testTarget):

    print("Generando modelos de regresión polinomiales")
    modelSummary = []

    for i in range(2,6):

        print(f"Generando regresión polinomial de grado {i}")

        if (i == 5):
            trainInput = trainInput[0:200]
            trainTarget = trainTarget[0:200]

        interaction = PolynomialFeatures(degree=i)
        polyTrainInput = interaction.fit_transform(trainInput)
        interaction.fit(polyTrainInput, trainTarget)
        model = LinearRegression()
        model.fit(polyTrainInput, trainTarget)
        modelSummary.append(crossValidation(model, f"Polinomial grado {i}",
                                            polyTrainInput, trainTarget))
        if (i != 5):
            targetPrediction = model.predict(interaction.fit_transform(testInput))
            finalTestError = mean_squared_error(testTarget, targetPrediction)
            modelSummary[i-2].append(finalTestError)
            print(f"MSE en validación 80/20: {finalTestError}")

    return modelSummary


def ridgeModel(trainInput, trainTarget, testInput, testTarget):

    print("Generando modelo Ridge regression")

    model = Ridge()
    model.fit(trainInput, trainTarget)
    modelSummary = crossValidation(model, "Ridge Regression", trainInput, trainTarget)
    targetPrediction = model.predict(testInput)
    finalTestError = mean_squared_error(testTarget, targetPrediction)
    modelSummary.append(finalTestError)
    print(f"MSE en validación 80/20: {finalTestError}")

    return modelSummary


def logisticModel(trainInput, trainTarget, testInput, testTarget):

    print("Generando modelo de regresión logística")

    scaler = StandardScaler()
    lr = LogisticRegression()
    model = Pipeline([("standardize", scaler),
                     ("log_reg", lr)])
    model.fit(trainInput, trainTarget.astype('int'))
    modelSummary = crossValidation(model, "Logistic Regression", trainInput, trainTarget.astype('int'))
    targetPrediction = model.predict(testInput)
    finalTestError = mean_squared_error(testTarget, targetPrediction)
    modelSummary.append(finalTestError)
    print(f"MSE en validación 80/20: {finalTestError}")

    return modelSummary

