import pandas as pd
import preprocess
import regressionModels

COLS = []

def exportCSV(data):
    pd.DataFrame(data).to_csv(r"./models_result.csv", index=False, header=True)


def readDataFile():
    try:
        rawData = pd.read_csv("./Train.csv")
    except IOError:
        print("No se ha encontrado el archivo de datos, intentando desde web")

        try:
            url = "https://gitlab.com/ALobhos/regressionmodelstask/-/raw/main/Train.csv"
            rawData = pd.read_csv(url, header=None)
        except:
            print("No se puede conectar con el servidor web. Compruebe su "
                  "conexión a internet e intente denuevo")
            exit(1)


    print(f"Filas y columnas de dataset: {rawData.shape}")
    print(f"Campos presentes en dataset: {rawData.columns}")
    print(f"Campo target: tumor_size")
    
    return rawData


def dataset_information(rawData):

    for col in rawData.columns:
        COLS.append(col)
        print("+----------------------------------+")
        print(rawData[[col]].describe())
        print("+----------------------------------+")
    
    
    print("Comprobar existencia de datos nulos o faltantes")
    print(rawData[rawData.isnull()].any())
    print("Se evidencia la no existencia de datos nulos o faltantes")


def generateRegressionModels():

    resultData = [["Modelo", "Set 1", "Set 2", "Set 3", "Set 4", "Set 5",
                   "MSE Promedio", "Desviación estándar MSE",
                   "Validación 80/20"]]
    rawData = readDataFile()
    dataset_information(rawData)
    normalizedData = preprocess.normalizeData(rawData, COLS)
    cleanInput, cleanTarget = preprocess.dataset_cleaner(normalizedData, COLS)
    trainInput, trainTarget, testInput, testTarget = preprocess.generateSamples(cleanInput, cleanTarget)

    logInputClean, logTargetClean = preprocess.dataset_cleaner(rawData, COLS)
    logTrainInput, logTrainTarget, logTestInput, logTestTarget = preprocess.generateSamples(logInputClean,
                                                                                            logTargetClean)
    resultData.append(regressionModels.linealModel(trainInput, trainTarget,
                                                   testInput, testTarget))

    polynomialSummary = regressionModels.polynomialModel(trainInput,
                                                         trainTarget,
                                                         testInput,
                                                         testTarget)

    for x in polynomialSummary:
        resultData.append(x)


    resultData.append(regressionModels.ridgeModel(trainInput, trainTarget,
                                                  testInput, testTarget))

    resultData.append(regressionModels.logisticModel(logTrainInput, logTrainTarget,
                                                     logTestInput, logTestTarget))

    exportCSV(resultData)


if __name__ == "__main__":
    generateRegressionModels()
    
    
